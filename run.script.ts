import buildAppBoundle from "./build.app.bundle.ts";
import type { ServerConfig } from "./http-server/server-config.ts";
import type { HttpServer } from "./http-server/http-server.ts";
import DenoHttpServer from "./http-server/std-http-server.ts";
import OakHttpServer from "./http-server/oak-server.ts";
import { renderOnServerSide } from "./react-app/server-side-rendering.jsx";

type HttpServerBuilder = (config: ServerConfig, prerenderedBody: string) => HttpServer

const httpServerBuilder = await createHttpServerBaseOnInputArgs()
if (httpServerBuilder) {
  const config = await readConfiguration()
  await buildAppBoundle(`${config.resourcePath}/${config.bundleJs}`);
  const prerenderedBody: string = await renderOnServerSide();
  const httpServer = httpServerBuilder(config, prerenderedBody)
  httpServer.run()
}

async function readConfiguration(): Promise<ServerConfig> {
  const config: ServerConfig = JSON.parse(
    await Deno.readTextFile("./config.json"),
  );
  config.resourcePath = `${Deno.cwd()}/public`;
  return config;
}

async function createHttpServerBaseOnInputArgs(): Promise<HttpServerBuilder | undefined> {
  const httpServerType = readHttpServerTypeFromInputArgs()
  if (httpServerType === "std") {
    return (config: ServerConfig, prerenderedBody: string) => new DenoHttpServer(config, prerenderedBody)
  } else if (httpServerType === "oak") {
    return (config: ServerConfig, prerenderedBody: string) => new OakHttpServer(config, prerenderedBody)
  } else {
    console.error(`unknown server has been chosen: ${httpServerType}`);
    return undefined
  }
}

function readHttpServerTypeFromInputArgs(): string {
  const args = Deno.args;
  if (args.length > 0) {
    return args[0]
  }
  return "std";
}
