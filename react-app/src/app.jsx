import { React } from "../../deps.ts";

const App = () => {
  const [count, setCount] = React.useState(0);
  return (
    <React.Fragment>
      <h1>Hello Deno!</h1>
      <button onClick={() => setCount(count + 1)}>Click the 🦕</button>
      <p>You clicked the 🦕 {count} times</p>
    </React.Fragment>
  );
};

export default App;
