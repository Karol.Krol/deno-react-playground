import { React, ReactDOM } from "../deps.ts";
import App from "./src/app.jsx";

ReactDOM.hydrate(
  <App />,
  // deno-lint-ignore no-undef
  document.getElementById("app"),
);
