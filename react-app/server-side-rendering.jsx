import { React, ReactDOMServer } from "../deps.ts";

import App from "./src/app.jsx";

function renderOnServerSide() {
  return ReactDOMServer.renderToString(
    <App />,
  );
}

export { renderOnServerSide };
