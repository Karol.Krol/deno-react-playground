async function buildClientsBundle(
  bundlePath: string,
) {
  const [diagnostic, bundle] = await Deno.bundle(
    "react-app/client-side-rendering.jsx",
    undefined,
    {
      lib: ["dom", "dom.iterable", "esnext"],
    },
  );
  if (diagnostic) {
    console.log(diagnostic);
  }
  Deno.writeFile(bundlePath, new TextEncoder().encode(bundle));
  console.log(`Generate boudle ${bundlePath}`);
}

export default buildClientsBundle;
