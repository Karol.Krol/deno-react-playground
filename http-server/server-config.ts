type ServerConfig = {
  addres: string;
  port: number;
  bundleJs: string;
  resourcePath: string;
};

export type { ServerConfig };
