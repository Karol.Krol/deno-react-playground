import {
  ServerRequest,
  listenAndServe,
} from "https://deno.land/std@0.70.0/http/server.ts";
import { serveFile } from "https://deno.land/std@0.70.0/http/file_server.ts";
import type { ServerConfig } from "./server-config.ts";
import type { HttpServer } from "./http-server.ts";
import { renderPage, fileExists } from "./http-server.ts";

class DenoHttpServer implements HttpServer {
  private config: ServerConfig;
  private initialBody: string;

  constructor(config: ServerConfig, initialBody: string) {
    this.config = config;
    this.initialBody = initialBody;
  }

  async run() {
    const { resourcePath, port, addres } = this.config;
    const html = await Deno.readTextFile(`${resourcePath}/index.html`);
    const prerenderedHtml = await renderPage(
      html,
      "#Placeholder#",
      this.initialBody,
    );

    listenAndServe(
      { port: port, hostname: addres },
      async (req: ServerRequest) => {
        const filePath = `${resourcePath}/${req.url}`;
        if (await fileExists(filePath)) {
          const content = await serveFile(req, filePath);
          req.respond(content);
        } else if (req.url === "/") {
          req.respond({
            body: prerenderedHtml,
            headers: new Headers({ "Content-Type": "text/html" }),
          });
        } else {
          req.respond({ status: 404 });
        }
      },
    );
    console.log(
      `Start std http server on address: http://${addres}:${port}`,
    );
  }
}

export default DenoHttpServer;
