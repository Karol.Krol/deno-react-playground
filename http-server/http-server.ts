interface HttpServer {
  run(): void;
}

async function renderPage(
  html: string,
  placeholder: string,
  replacement: string,
): Promise<string> {
  return html.replace(placeholder, replacement);
}

async function fileExists(path: string): Promise<boolean> {
  try {
    const stats = await Deno.lstat(path);
    return stats && stats.isFile;
  } catch (e) {
    if (e && e instanceof Deno.errors.NotFound) {
      return false;
    } else {
      throw e;
    }
  }
}

export type { HttpServer };
export { renderPage, fileExists };
