import {
  Application,
  Router,
  Context,
  send,
} from "https://deno.land/x/oak@v6.2.0/mod.ts";
import type { ServerConfig } from "./server-config.ts";
import type { HttpServer } from "./http-server.ts";
import { renderPage, fileExists } from "./http-server.ts";

class OakHttpServer implements HttpServer {
  private config: ServerConfig;
  private initialBody: string;

  constructor(config: ServerConfig, initialBody: string) {
    this.config = config;
    this.initialBody = initialBody;
  }

  async run() {
    const { port, addres } = this.config;
    const router = await this.setupRouter();
    const app: Application = new Application();
    app.use(this.staticFileMiddleware);
    app.use(router.routes());
    app.use(router.allowedMethods());
    app.addEventListener("listen", () => {
      console.log(
        `Start oak server on address: http://${addres}:${port}`,
      );
    });
    await app.listen({
      port,
      hostname: addres,
    });
  }
  private async setupRouter(): Promise<Router> {
    const { resourcePath } = this.config;
    const router = new Router();
    const html = await Deno.readTextFile(`${resourcePath}/index.html`);
    const prerenderedHtml = await renderPage(
      html,
      "#Placeholder#",
      this.initialBody,
    );
    router
      .get("/", async (context: Context) => {
        context.response.type = "text/html";
        context.response.body = prerenderedHtml;
      });
    return router;
  }

  private staticFileMiddleware = async (ctx: Context, next: Function) => {
    const path = `${this.config.resourcePath}/${ctx.request.url.pathname}`;
    if (await fileExists(path)) {
      await send(ctx, ctx.request.url.pathname, {
        root: `${this.config.resourcePath}`,
        index: "index.html",
      });
    } else {
      await next();
    }
  };
}

export default OakHttpServer;
